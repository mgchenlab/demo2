from django.shortcuts import render, redirect
from django.urls import reverse
from django.http import JsonResponse
from django.contrib.contenttypes.models import ContentType
#from django.utils.html import strip_tags #把html的tag過濾，只留文字的部分
#from notifications.signals import notify
from .models import Comment
from .forms import CommentForm

# Create your views here.
def update_comment(request):
    referer = request.META.get('HTTP_REFERER', reverse('home'))
    comment_form = CommentForm(request.POST, user=request.user)

    data = {}
    if comment_form.is_valid():
        #資料插入
        comment = Comment()
        comment.user = comment_form.cleaned_data['user']
        comment.text = comment_form.cleaned_data['text']
        comment.content_object = comment_form.cleaned_data['content_object']

        parent = comment_form.cleaned_data['parent']
        if not parent is None:
            comment.root = parent.root if not parent.root is None else parent
            comment.parent = parent
            comment.reply_to = parent.user
            data['reply_to'] = comment.reply_to.get_nickname_or_username()
        else:
            data['reply_to'] = ''

        comment.save()

        '''移至signal，讓耦合性降低。
        #發送消息通知
        if comment.reply_to is None:
            #評論
            if comment.content_type.model == 'blog':
                blog = comment.content_object
                recipient = comment.content_object.get_user()
                verb = '{0}評論了你的[{1}]'.format(comment.user.get_nickname_or_username(), blog.title)
            else:
                raise Exception('unknown comment object type')
        else:
            #回覆
            recipient = comment.reply_to
            verb = '{0}評論了你的[{1}]'.format(comment.user.get_nickname_or_username(), strip_tags(comment.parent.text))


        notify.send(comment.user, recipient=recipient, verb=verb, action_object=comment)
        '''

        data['pk'] = comment.pk
        data['status'] = 'SUCCESS'
        data['username'] = comment.user.get_nickname_or_username()
        data['created_at'] = comment.created_at.now().strftime('%Y-%m-%d %H:%M:%S')
        data['text'] = comment.text
        data['root_pk'] = comment.root.pk if not comment.root is None  else ''
        data['content_type'] = ContentType.objects.get_for_model(comment).model
    else:
        data['status'] = 'ERROR'
        data['message'] = list(comment_form.errors.values())[0][0]


    return JsonResponse(data)
