from django.utils.html import strip_tags #把html的tag過濾，只留文字的部分
from django.db.models.signals import post_save
from django.dispatch import receiver
from notifications.signals import notify
from .models import Comment

@receiver(post_save, sender=Comment) #comment save 會執行此signal
def send_notification(sender, instance, **kwargs):
    #發送消息通知
    if instance.reply_to is None:
        #評論
        if instance.content_type.model == 'blog':
            blog = instance.content_object
            recipient = instance.content_object.get_user()
            verb = '{0}評論了你的文章[{1}]'.format(instance.user.get_nickname_or_username(), blog.title)
        else:
            raise Exception('unknown comment object type')
    else:
        #回覆
        recipient = instance.reply_to
        verb = '{0}回覆了你的留言[{1}]'.format(instance.user.get_nickname_or_username(), strip_tags(instance.parent.text))

    url = instance.content_object.get_url() + "#comment_" + str(instance.pk)
    notify.send(instance.user, recipient=recipient, verb=verb, action_object=instance, url=url)#此url是自定義參數