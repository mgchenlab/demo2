from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User

# Create your models here.
class Comment(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, related_name='comments', on_delete=models.CASCADE)

    reply_to = models.ForeignKey(User, related_name='reply_comments', null=True, on_delete=models.CASCADE) #回覆誰
    root = models.ForeignKey('self', related_name='root_comments', null=True, on_delete=models.CASCADE) #頂評論
    parent = models.ForeignKey('self', related_name='parent_comment', null=True, on_delete=models.CASCADE) #上一層評論

    def __str__(self):
        return self.text

    def get_user(self):
        return self.user

    def get_url(self):
        return self.content_object.get_url()

    class Meta:
        ordering = ['created_at']

