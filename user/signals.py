from django.db.models.signals import post_save
from django.dispatch import receiver
from notifications.signals import notify
from django.urls import reverse
from django.contrib.auth.models import User

@receiver(post_save, sender=User) #comment save 會執行此signal
def send_notification(sender, instance, **kwargs):
    if kwargs['created'] == True: #判斷儲存user為create的時候
        verb = '註冊成功，歡迎加入。'
        url = reverse('user_info')
        notify.send(instance, recipient=instance, verb=verb, action_object=instance, url=url)#此url是自定義參數