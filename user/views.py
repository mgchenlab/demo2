import threading
import string
import random
import time
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.urls import reverse
from django.contrib import auth
from django.conf import settings
from django.core.mail import send_mail
from django.contrib.auth.models import User
from .forms import LoginForm, RegisterForm, ChangeNicknameForm, BindEmailForm, ChangePasswordForm, ForgetPasswordForm
from .models import Profile

#多線程發送信件
class SendMailThread(threading.Thread):
    def __init__(self, subject, text, emails, send_for, fail_silently=False):
        self.subject = subject
        self.text = text
        self.emails = emails
        self.fail_silently = fail_silently
        #判斷此為何種驗證碼
        self.send_for = send_for
        threading.Thread.__init__(self)

    def run(self):
        #發送信箱
        send_mail(
            self.subject,
            '',
            settings.EMAIL_HOST_USER,
            self.emails,
            fail_silently=self.fail_silently,
            html_message=self.text,
        )


def login(request):
    if request.method == 'POST':
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            user = login_form.cleaned_data['user']
            auth.login(request, user)
            return redirect(reverse('home'))
    else:
        login_form = LoginForm()

    context = {}
    context['login_form'] = login_form
    return render(request, 'user/login.html', context)

def login_for_modal(request):
    login_form = LoginForm(request.POST)
    data={}
    if login_form.is_valid():
        user = login_form.cleaned_data['user']
        auth.login(request, user)
        data['status'] = "SUCCESS"
    else:
        data['status'] = "ERROR"
    return JsonResponse(data)

def logout(request):
    auth.logout(request)
    return redirect(reverse('home'))

def register(request):
    if request.method == 'POST':
        register_form = RegisterForm(request.POST, request=request)
        if register_form.is_valid():
            username = register_form.cleaned_data['username']
            email = register_form.cleaned_data['email']
            password = register_form.cleaned_data['password']
            #新增資料
            # user = User()
            # user.username = username
            # user.email = email
            # user.set_password(password) #密碼加密
            # user.save()

            user = User.objects.create_user(username, email, password)
            user.save()
            #清除session
            del request.session['register_code']
            #登入
            user = auth.authenticate(username=username, password=password)
            auth.login(request, user)

            return redirect(reverse('home'))
    else:
        register_form = RegisterForm()

    context = {}
    context['register_form'] = register_form
    return render(request, 'user/register.html', context)

def user_info(request):
    context = {}
    return render(request, 'user/user_info.html', context)

def change_nickname(request):
    if request.method == 'POST':
        form = ChangeNicknameForm(request.POST, user=request.user)
        if form.is_valid():
            nickname_new = form.cleaned_data['nickname_new']
            profile, created = Profile.objects.get_or_create(user=request.user)
            profile.nickname = nickname_new
            profile.save()
            return redirect(reverse('user_info'))
    else:
        form = ChangeNicknameForm()

    context = {}
    context['page_title'] = '修改暱稱'
    context['form_title'] = '修改暱稱'
    context['submit_text'] = '修改'
    context['form'] = form
    context['back_url'] = reverse('user_info')
    return render(request, 'form.html', context)

def bind_email(request):
    if request.method == 'POST':
        form = BindEmailForm(request.POST, request=request)
        if form.is_valid():
            email = form.cleaned_data['email']
            request.user.email = email
            request.user.save()
            #清除session
            del request.session['bind_email_code']
            return redirect(reverse('user_info'))
    else:
        form = BindEmailForm()

    context = {}
    context['page_title'] = '綁定信箱'
    context['form_title'] = '綁定信箱'
    context['submit_text'] = '綁定'
    context['form'] = form
    context['back_url'] = reverse('user_info')
    return render(request, 'user/bind_email_form.html', context)

def send_verification_code(request):
    email = request.GET.get('email', '')
    send_for = request.GET.get('send_for')
    data = {}

    if send_for != 'bind_email_code' and send_for != 'register_code' and send_for != 'forget_password_code':
        data['status'] = 'ERROR'
        data['message'] = '發送錯誤'
        return JsonResponse(data)

    if email == '':
        data['status'] = 'ERROR'
        data['message'] = '信箱不能為空'
        return JsonResponse(data)

    if User.objects.filter(email=email).exists() and send_for != 'forget_password_code':
        data['status'] = 'ERROR'
        data['message'] = '信箱已被註冊'
        return JsonResponse(data)



    #避免用戶頻繁的發送郵件，導致伺服器過載。
    now = int(time.time())
    send_code_time = request.session.get('send_code_time', 0)
    left_time = now - send_code_time
    if left_time < 30:
        data['status'] = 'ERROR'
        data['message'] = '30秒內不能重複發送驗證碼，還有%s秒' % left_time
        return JsonResponse(data)

    #生成驗證碼
    code = ''.join(random.sample(string.ascii_letters + string.digits, 4))

    #記錄在session
    request.session[send_for] = code
    request.session['send_code_time'] = now

    #發送信箱
    '''
    send_mail(
        '綁定信箱',
        '驗證碼: %s' % code,
        '',
        [email],
        fail_silently=False,
    )
    '''

    context = {}
    context['send_for'] = send_for
    context['verification_code'] = code
    context['index_url'] = request.META['HTTP_HOST']

    text = render_to_string('email/verification.html', context)

    send_mail_thread = SendMailThread('綁定信箱', text, [email], send_for)
    send_mail_thread.start()

    data['status'] = 'SUCCESS'

    return JsonResponse(data)

def change_password(request):
    if request.method == 'POST':
        form = ChangePasswordForm(request.POST, user=request.user)
        if form.is_valid():
            new_password = form.cleaned_data['new_password']
            user = request.user
            user.set_password(new_password)
            user.save()
            auth.logout(request)
            return redirect(reverse('home'))
    else:
        form = ChangePasswordForm()

    context = {}
    context['page_title'] = '修改密碼'
    context['form_title'] = '修改密碼'
    context['submit_text'] = '修改'
    context['form'] = form
    context['back_url'] = reverse('home')
    return render(request, 'form.html', context)

def forget_password(request):
    if request.method == 'POST':
        form = ForgetPasswordForm(request.POST, request=request)
        if form.is_valid():
            email = form.cleaned_data['email']
            new_password = form.cleaned_data['new_password']
            user = User.objects.get(email=email)
            user.set_password(new_password)
            user.save()
            #清除session
            del request.session['forget_password_code']
            return redirect(reverse('login'))
    else:
        form = ForgetPasswordForm()

    context = {}
    context['page_title'] = '重置密碼'
    context['form_title'] = '重置密碼'
    context['submit_text'] = '重置'
    context['form'] = form
    context['back_url'] = reverse('home')
    return render(request, 'user/forget_password_form.html', context)

