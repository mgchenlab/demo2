import datetime
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from django.db.models import Sum
from .models import ReadCount, ReadDetail

def get_key_and_excute_once_statistics(request, obj):
    ct = ContentType.objects.get_for_model(obj)
    key = "%s_%s_read" % (ct.model, obj.pk)
    if not request.COOKIES.get(key):
        read_count, read_count_created = ReadCount.objects.get_or_create(content_type=ct, object_id=obj.pk)

        date = timezone.localtime().date()#抓取時區的現在日期

        read_detail, read_detail_created = ReadDetail.objects.get_or_create(content_type=ct, object_id=obj.pk, date=date)

        #總閱讀數+1
        read_count.read_count += 1
        read_count.save()
        #當天閱讀數+1
        read_detail.read_count += 1
        read_detail.save()

    return key

def get_read_detail_before_7day(content_type):
    today = timezone.localtime().date()#抓取時區的現在日期

    date_list = []
    read_count_list = []
    for i in range(6, -1, -1):
        date = today - datetime.timedelta(days=i)
        date_list.append(date.strftime('%m/%d'))
        read_details = ReadDetail.objects.filter(content_type=content_type, date=date)
        result = read_details.aggregate(read_count_sum=Sum('read_count'))
        read_count_list.append(result['read_count_sum'] or 0)
    return date_list, read_count_list
