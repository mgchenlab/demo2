from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
#from ckeditor.fields import RichTextField #不允許上傳文件
from django.contrib.contenttypes.fields import GenericRelation
from ckeditor_uploader.fields import RichTextUploadingField #允許上傳文件
from read_statistics.models import ReadDetail, ReadCountExpandMethod

class BlogType(models.Model):
    type_name = models.CharField(max_length=15)

    def __str__(self):
        return self.type_name

class Blog(models.Model, ReadCountExpandMethod):
    title = models.CharField(max_length=30)#由於sqllite的編碼與msyql的utf8mb4不同，導入至msyql時，會有數據過長的問題
    blog_type = models.ForeignKey(BlogType, on_delete=models.CASCADE)
    content = RichTextUploadingField() #RichTextField()
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    read_details = GenericRelation(ReadDetail)
    def __str__(self):
        return "<Blog: %s>" % self.title

    def get_user(self):
        return self.author

    def get_url(self):
        return reverse('blog_detail', kwargs={'blog_pk': self.pk})

    class Meta:
        ordering = ['-created_at']