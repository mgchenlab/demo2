from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator
from django.db.models import Count
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from read_statistics.utils import get_key_and_excute_once_statistics
#from comment.models import Comment
#from comment.forms import CommentForm
#from user.forms import LoginForm
from .models import Blog, BlogType

each_page_number = settings.EACH_PAGE_NUMBER

#有加頁碼的blog list data
def get_blog_list_common_data(request, blogs):
    page_number = request.GET.get('page', 1)#獲取get參數，若為空，預設為1
    paginator = Paginator(blogs, each_page_number)
    page = paginator.get_page(page_number)#get_page方法，若為不合法參數，比如小於最小頁或非數字，則返回第一頁資料，超過最多頁，則返回最後一頁資料。
    page_number = page.number;#當前頁碼
    page_range = list(range(max(page_number - 2, 1), page_number)) + \
                 list(range(page_number, min(page_number + 2, paginator.num_pages) + 1))

    if page_range[0] - 1 > 2:
        page_range.insert(0, '...')
    if paginator.num_pages - page_range[-1] >=2:
        page_range.append('...')

    if page_range[0] != 1:
        page_range.insert(0, 1)
    if page_range[-1] != paginator.num_pages:
        page_range.append(paginator.num_pages)

    #獲取日期統計
    blog_dates = Blog.objects.dates('created_at', 'month', order="DESC")
    blog_dates_dict = {}
    for blog_date in blog_dates:
        blog_count = Blog.objects.filter(created_at__year=blog_date.year,
                                         created_at__month=blog_date.month).count()
        blog_dates_dict[blog_date] = blog_count

    context = {}
    context['page'] = page
    context['page_range'] = page_range
    context['blogs'] = page.object_list
    context['blog_types'] = BlogType.objects.annotate(blog_count=Count('blog')) #annotate:附值, Count('blog'): 因為BlogType可以反查Blog，預設為blog，若要修改，則要去model的ForeignKey多加一個related_name參數
    context['blog_dates'] = blog_dates_dict

    return context

# Create your views here.
def blog_list(request):
    blogs = Blog.objects.all()
    context = get_blog_list_common_data(request, blogs)
    return render(request, 'blog/blog_list.html', context)

def blog_type(request, blog_type_pk):
    blog_type = get_object_or_404(BlogType, pk=blog_type_pk)
    blogs = Blog.objects.filter(blog_type=blog_type)

    context = get_blog_list_common_data(request, blogs)
    context['blog_type'] = blog_type
    return render(request, 'blog/blog_type.html', context)

def blog_date(request, year, month):
    blogs = Blog.objects.filter(created_at__year=year, created_at__month=month)
    context = get_blog_list_common_data(request, blogs)
    context['blog_date'] = '%s年%s月' % (year, month)
    return render(request, 'blog/blog_date.html', context)

def blog_detail(request, blog_pk):

    blog = get_object_or_404(Blog, pk=blog_pk)
    read_cookie_key = get_key_and_excute_once_statistics(request, blog)

    context = {}
    context['blog'] = blog
    context['previous_blog'] = Blog.objects.filter(created_at__gt=blog.created_at).last()
    context['next_blog'] = Blog.objects.filter(created_at__lt=blog.created_at).first()

    #此表單已加入context processor裡
    #context['login_form'] = LoginForm()

    #移至template_tags 降低耦合性
    #blog_content_type = ContentType.objects.get_for_model(blog)
    #comments = Comment.objects.filter(content_type=blog_content_type, object_id=blog.pk, parent=None)
    #context['comments'] = comments.order_by('-created_at')
    #context['comment_count'] = Comment.objects.filter(content_type=blog_content_type, object_id=blog.pk).count()
    #context['comment_form'] = CommentForm(initial={'content_type': blog_content_type.model, 'object_id': blog_pk, 'reply_comment_id': 0})

    response =  render(request, 'blog/blog_detail.html', context)
    response.set_cookie(read_cookie_key, 'true')
    return response
