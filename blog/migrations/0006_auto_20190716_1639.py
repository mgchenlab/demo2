# Generated by Django 2.1.7 on 2019-07-16 08:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_blog_read_count'),
    ]

    operations = [
        migrations.CreateModel(
            name='ReadCount',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('read_count', models.IntegerField(default=0)),
            ],
        ),
        migrations.RemoveField(
            model_name='blog',
            name='read_count',
        ),
        migrations.AddField(
            model_name='readcount',
            name='blog',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='blog.Blog'),
        ),
    ]
