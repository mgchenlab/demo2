"""
WSGI config for demo2 project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'demo2.settings.production')

application = get_wsgi_application()

#有修改程式的時候，讓uwsgi自動reload(測試階段)
'''
try:
    import uwsgi
    from uwsgidecorators import timer
    from django.utils import autoreload

    @timer(3)
    def change_code_graceful_reload(sig):
        if autoreload.code_changed():
            uwsgi.reload()
except ImportError:
    # not running under uwsgi
    pass
'''