import datetime
from django.core.paginator import Paginator
from django.utils import timezone
from django.shortcuts import render, redirect, get_object_or_404
from django.core.cache import cache
from django.contrib.contenttypes.models import ContentType
from django.db.models import Sum, Q
from read_statistics.utils import get_read_detail_before_7day
from blog.models import Blog

def test_email(request):
    from django.core.mail import send_mail
    from django.http import HttpResponse
    from django.conf import settings

    send_mail(
        '測試信件',
        '測試內容',
        settings.EMAIL_HOST_USER,
        ['mgchen.work3@gmail.com'],
        False,
    )
    return HttpResponse('success')

def get_blog_before_day_count(before_day_count):
    today = timezone.now().date()
    date = today - datetime.timedelta(days=before_day_count)
    read_details = Blog.objects \
                       .filter(read_details__date__range=(date, today)) \
                       .values('id', 'title') \
                       .annotate(read_count_sum=Sum('read_details__read_count')) \
                       .order_by("-read_count_sum")
    return read_details[:7] #sql:limit 0~7

def home(request):
    blog_content_type = ContentType.objects.get_for_model(Blog)
    date_list, read_count_list = get_read_detail_before_7day(blog_content_type)

    week_blog_list = cache.get('week_blog_list')
    if week_blog_list is None:
        week_blog_list = get_blog_before_day_count(7)
        cache.set('week_blog_list', week_blog_list, 3600)

    context = {}
    context['date_list'] = date_list
    context['read_count_list'] = read_count_list
    context['today_blog_list'] = get_blog_before_day_count(0)
    context['yesterday_blog_list'] = get_blog_before_day_count(1)
    context['week_blog_list'] = week_blog_list

    return render(request, 'home.html', context)

def search(request):
    search_words = request.GET.get('word', '').strip()
    #製作空格搜尋
    #Q function ~ & |
    conditions = None
    for word in search_words.split(' '):
        if conditions is None:
            conditions = Q(title__icontains=word)
        else:
            conditions = conditions | Q(title__icontains=word)

    search_blogs = []
    if not conditions is None:
        search_blogs = Blog.objects.filter(conditions)

    #分頁
    page_number = request.GET.get('page', 1)#獲取get參數，若為空，預設為1
    paginator = Paginator(search_blogs, 20)
    page = paginator.get_page(page_number)#get_page方法，若為不合法參數，比如小於最小頁或非數字，則返回第一頁資料，超過最多頁，則返回最後一頁資料。

    print(search_blogs)

    context = {}
    context['search_words'] = search_words
    context['search_blogs'] = page
    context['search_blogs_count'] = search_blogs.count()
    return render(request, 'search.html', context)


'''
def my_notifications(request):
    context = {}
    return render(request, 'my_notifications.html', context)

def my_notification(request, my_notification_pk):
    my_notification = get_object_or_404(Notification, pk=my_notification_pk)
    my_notification.unread = False
    my_notification.save()
    return redirect(my_notification.data['url'])
'''

'''
def login(request):
    if request.method == 'POST':
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            user = login_form.cleaned_data['user']
            auth.login(request, user)
            return redirect(reverse('home'))
    else:
        login_form = LoginForm()

    context = {}
    context['login_form'] = login_form
    return render(request, 'login.html', context)

def login_for_modal(request):
    login_form = LoginForm(request.POST)
    data={}
    if login_form.is_valid():
        user = login_form.cleaned_data['user']
        auth.login(request, user)
        data['status'] = "SUCCESS"
    else:
        data['status'] = "ERROR"
    return JsonResponse(data)

def logout(request):
    auth.logout(request)
    return redirect(reverse('home'))

def register(request):
    if request.method == 'POST':
        register_form = RegisterForm(request.POST)
        if register_form.is_valid():
            username = register_form.cleaned_data['username']
            email = register_form.cleaned_data['email']
            password = register_form.cleaned_data['password']
            #新增資料
            # user = User()
            # user.username = username
            # user.email = email
            # user.set_password(password) #密碼加密
            # user.save()

            user = User.objects.create_user(username, email, password)
            user.save()
            #登入
            user = auth.authenticate(username=username, password=password)
            auth.login(request, user)

            return redirect(reverse('home'))
    else:
        register_form = RegisterForm()

    context = {}
    context['register_form'] = register_form
    return render(request, 'register.html', context)

def user_info(request):
    context = {}
    return render(request, 'user_info.html', context)
'''
