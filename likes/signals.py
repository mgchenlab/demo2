from django.utils.html import strip_tags #把html的tag過濾，只留文字的部分
from django.db.models.signals import post_save
from django.dispatch import receiver
from notifications.signals import notify
from .models import LikeRecord

@receiver(post_save, sender=LikeRecord) #comment save 會執行此signal
def send_notification(sender, instance, **kwargs):
    if instance.content_type.model == 'blog':
        blog = instance.content_object
        verb = '{0}點讚了你的文章[{1}]'.format(instance.user.get_nickname_or_username(), blog.title)
    elif instance.content_type.model == 'comment':
        comment = instance.content_object
        verb = '{0}點讚了你的留言[{1}]'.format(instance.user.get_nickname_or_username(), strip_tags(comment.text))
    print(instance.content_object.get_url())
    url = instance.content_object.get_url()
    recipient = instance.content_object.get_user()

    notify.send(instance.user, recipient=recipient, verb=verb, action_object=instance, url=url)#此url是自定義參數