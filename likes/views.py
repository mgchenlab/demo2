from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.contenttypes.models import ContentType
from django.db.models import ObjectDoesNotExist
from .models import LikeCount, LikeRecord

def SuccessResponse(liked_count):
    data = {}
    data['status'] = 'SUCCESS'
    data['liked_count'] = liked_count
    return JsonResponse(data)

def ErrorResponse(code, message):
    data = {}
    data['status'] = 'ERROR'
    data['code'] = code
    data['message'] = message
    return JsonResponse(data)

# Create your views here.
def like_change(request):
    user = request.user
    if not user.is_authenticated:
        return ErrorResponse(400, 'you were not login')

    content_type = request.GET.get('content_type')
    object_id = int(request.GET.get('object_id'))

    try:
        content_type = ContentType.objects.get(model=content_type)
        model_class = content_type.model_class()
        model_obj = model_class.objects.get(pk=object_id)
    except ObjectDoesNotExist:
        return ErrorResponse(401, 'object not exist')

    if request.GET.get('is_like') == 'true':
        #要點讚
        like_record, created = LikeRecord.objects.get_or_create(content_type=content_type, object_id=object_id, user=user)
        if created:
            #未點讚過
            like_count, created = LikeCount.objects.get_or_create(content_type=content_type, object_id=object_id)
            like_count.liked_count += 1
            like_count.save()
            return SuccessResponse(like_count.liked_count)
        else:
            #已點過讚
            return ErrorResponse(402, 'you were liked')
    else:
        #取消點讚
        if LikeRecord.objects.filter(content_type=content_type, object_id=object_id, user=user).exists():
            #有點贊過
            like_record = LikeRecord.objects.get(content_type=content_type, object_id=object_id, user=user)
            like_record.delete()

            like_count, created = LikeCount.objects.get_or_create(content_type=content_type, object_id=object_id)
            if not created:
                like_count.liked_count -= 1
                like_count.save()
                return SuccessResponse(like_count.liked_count)
            else:
                return ErrorResponse(402, 'data error')
        else:
            #未點讚過
            return ErrorResponse(402, 'you were not liked')
